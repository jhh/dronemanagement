set -e

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    echo "Try to run \"source ${0}\" instead."
    exit 1
fi

coverage run --source=dronemanagement --omit=dronemanagement/__init__.py -m pytest
coverage report
coverage html --directory=dronemanagement/static/htmlcov
