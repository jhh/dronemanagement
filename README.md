Flask example for Drone management.

# Go.

```bash
docker-compose up --build
```

# Routes to explore.

/  
/create  
/get/<uuid:drone_id>  
/update/<uuid:drone_id>  
/delete  
/htmlcov  

For unit test code coverage, goto http://127.0.0.1:5000/htmlcov .

# File overview.

```
.
├── compose                                         # Docker related files.
│   └── Dockerfile
├── curl_test.sh                                    # Posts data to running flask app.
├── docker-compose.yml
├── dronemanagement                                 # Python project files.
│   ├── db.py
│   ├── entities.py                                 # Class(es).
│   ├── flaskapp.py                                 # Flask app.
│   ├── __init__.py
│   └── static
│       └── htmlcov                                 # Generated test coverage HTML.
├── execute_tests.sh                                # Unit tests and coverage script.
├── README.md                                       # You are <-- here.
├── requirements.txt
└── tests                                           # Unit tests.
    ├── __init__.py
    ├── test_entities.py
    └── test_flaskapp.py
```
