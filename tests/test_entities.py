import unittest, datetime

from dronemanagement.entities import Drone

class TestDrone(unittest.TestCase):

    def setUp(self):
        self.a_drone = Drone('foo-id', (0,0))

    def test_creating_a_drone(self):
        d = Drone('foo-id', (0,0))
        self.assertTrue(isinstance(d, Drone), "Created a Drone.")

    def test_getting_drone_location(self):
        self.assertEqual(self.a_drone.location, (0,0))

    def test_setting_drone_location(self):
        self.a_drone.location = (20, 30)
        self.assertEqual(self.a_drone.location, (20,30))

    def test_drone_id_is_read_only(self):
        self.assertEqual(self.a_drone.drone_id, 'foo-id', "Id can be read.")
        with self.assertRaises(AttributeError):
            self.a_drone.drone_id = 'baz-id'

    def test_drone_lift_off(self):
        self.assertEqual(self.a_drone.status, 'landed')
        self.a_drone.status = 'in-flight'
        self.assertEqual(self.a_drone.status, 'in-flight', "Drone lifted off.")

    def test_landing_drone(self):
        self.a_drone.status = 'in-flight'
        self.a_drone.status = 'landed'
        assert True, "Drone has landed."

    def test_applying_wrong_status(self):
        with self.assertRaises(AttributeError):
            self.a_drone.status = 'not a valid status'

    def test_eta_is_none_if_drone_has_landed(self):
        self.assertEqual(self.a_drone.status, 'landed')
        self.assertFalse(self.a_drone.eta)

    def test_eta_is_datetime_if_drone_is_inflight(self):
        self.a_drone.status = 'in-flight'
        self.assertTrue(isinstance(self.a_drone.eta, datetime.datetime))

