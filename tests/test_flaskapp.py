import os, tempfile, unittest, re, uuid
from flask.json import dumps, jsonify
from dronemanagement import flaskapp

def _prepare_app_and_database():
    flaskapp._clear_db()
    flaskapp.app.config['TESTING'] = True
    return flaskapp.app.test_client()

class TestAppWithEmptyDatabase(unittest.TestCase):

    def setUp(self):
        self.client = _prepare_app_and_database()

    def test_empty_index(self):
        data = self.client.get('/')
        self.assertEqual(data.json, [])

    def test_create_a_drone(self):
        post_data = dict(latitude=0, longitude=0)
        data = self.client.post('/create', data=post_data)
        self.assertEqual(
            set(data.json.keys()),
            set(['status', 'location', 'drone_id']),
            "Returned JSON keys matches expected values."
        )

class TestAppWithSomeItemsInDatabase(unittest.TestCase):

    def setUp(self):
        self.client = _prepare_app_and_database()
        _ = dict(latitude=0, longitude=0)
        data = self.client.post('/create', data=_)
        _ = dict(latitude=10, longitude=21)
        data = self.client.post('/create', data=_)

    def test_index_has_elements(self):
        data = self.client.get('/')
        self.assertTrue(len(data.json) == 2, "Data length checks out.")

    def test_elements_looks_like_drones(self):
        data = self.client.get('/')
        for d in data.json:
            self.assertEqual(
                set(d.keys()),
                set(['status', 'location', 'drone_id']),
                "Returned JSON keys matches expected values."
            )

    def test_deleting_unknown_nodeid(self):
        data = self.client.post('/delete', data=dict(drone_id='does not exist'))
        self.assertEqual(data.status_code, 400, "Bad request with unknown id")

    def test_delete_a_drone(self):
        a_drone = self.client.get('/').json[0]
        data = self.client.post(
            '/delete',
            data=dict(drone_id=a_drone['drone_id'])
        )
        self.assertEqual(data.status_code, 200, "Status code OK")
        self.assertTrue(
            re.match('^Drone #.+ deleted\.$', data.data.decode('utf-8')),
            "Recieved \"Drone #xx deleted.\" message."
        )

    def test_get_unknown_drone(self):
        get_path = '/get/{}'.format(uuid.uuid4())
        data = self.client.get(get_path)
        self.assertEqual(data.status_code, 404, "Not found.")

    def test_get_a_drone(self):
        a_drone_id = self.client.get('/').json[0]['drone_id']
        data = self.client.get('/get/{}'.format(a_drone_id))
        self.assertEqual(a_drone_id, data.json['drone_id'])

    def test_update_unknown_drone(self):
        update_path = '/update/{}'.format(uuid.uuid4())
        data = self.client.post(update_path, data=dict(
            status='in-flight',
            latitude=18,
            longitude=34
        ))
        self.assertEqual(data.status_code, 400, "Not found.")

    def test_update_drone(self):
        a_drone = self.client.get('/').json[0]
        update_path = '/update/{}'.format(a_drone['drone_id'])
        self.assertEqual(a_drone['status'], 'landed')
        self.assertEqual(int(a_drone['location']['latitude']), 0)
        self.assertEqual(int(a_drone['location']['longitude']), 0)

        # Validate response.
        data = self.client.post(update_path, data=dict(
            status='in-flight',
            latitude=18,
            longitude=34
        ))
        self.assertEqual(data.status_code, 200, "Update OK.")

        # Validate data.
        updated_drone = self.client.get('/get/{}'.format(a_drone['drone_id'])).json
        self.assertEqual(updated_drone['status'], 'in-flight')
        self.assertEqual(int(updated_drone['location']['latitude']), 18)
        self.assertEqual(int(updated_drone['location']['longitude']), 34)


