#!/bin/bash

echo See index.
curl http://127.0.0.1:5000/

echo
echo Create some Drones.
curl -X POST -F 'latitude=23' -F 'longitude=11' http://127.0.0.1:5000/create
curl -X POST -F 'latitude=2' -F 'longitude=19' http://127.0.0.1:5000/create
curl -X POST -F 'latitude=20' -F 'longitude=18' http://127.0.0.1:5000/create

echo
echo See index again.
curl http://127.0.0.1:5000/
