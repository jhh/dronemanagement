import uuid, os
import flask
from .entities import Drone, ToDictEncoder
from .db import _silly_db

app = flask.Flask(__name__)
app.json_encoder = ToDictEncoder

@app.route("/")
def index():
    return flask.jsonify(_silly_db)

@app.route("/create", methods = ['POST'])
def create():
    location = (
        float(flask.request.form['latitude']),
        float(flask.request.form['longitude'])
    )
    new_drone = Drone(str(uuid.uuid4()), location)
    _silly_db.append(new_drone)
    return flask.jsonify(new_drone)

@app.route("/get/<uuid:drone_id>")
def get(drone_id):
    drones = _db_lookup(str(drone_id))
    if len(drones) == 1:
        return flask.jsonify(drones[0])
    return '', 404

@app.route("/update/<uuid:drone_id>", methods = ['POST'])
def update(drone_id):
    drones = _db_lookup(str(drone_id))
    if len(drones) != 1:
        return '', 400
    drone = drones[0]

    if 'status' in flask.request.form:
        drone.status = flask.request.form['status']
    if 'latitude' in flask.request.form:
        drone.location = (
            float(flask.request.form['latitude']),
            drone.location[1]
        )
    if 'longitude' in flask.request.form:
        drone.location = (
            drone.location[0],
            float(flask.request.form['longitude'])
        )

    _ = 'Drone #{} updated.'.format(drone_id)
    return _, 200

@app.route("/delete", methods = ['POST'])
def delete():
    drones = _db_lookup(flask.request.form['drone_id'])
    if len(drones) == 1:
        _ = 'Drone #{} deleted.'.format(flask.request.form['drone_id'])
        return _, 200
    return '', 400  # Bad request.

@app.route('/htmlcov/')
def htmlcov_index(): # pragma: no cover
    return flask.redirect(flask.url_for('htmlcov', filename='index.html'))

@app.route('/htmlcov/<path:filename>')
def htmlcov(filename): # pragma: no cover
    module_dir = os.path.dirname(os.path.realpath(__file__))
    file_dir = os.path.join(module_dir, 'static', 'htmlcov')
    return flask.send_from_directory(file_dir, filename)

def _clear_db():
    _silly_db.clear()

def _db_lookup(drone_id):
    return [d for d in _silly_db if d.drone_id == drone_id]
