import datetime, json, collections

IN_FLIGHT = 'in-flight'
LANDED = 'landed'

class Drone():

    STATUSES = [IN_FLIGHT, LANDED]
    DEFAULT_STATUS = LANDED

    def __init__(self, drone_id, location, status=DEFAULT_STATUS):
        self._drone_id = drone_id
        self.location = location
        self.status = status

    def to_dict(self):
        return dict(
            drone_id=self.drone_id,
            status=self.status,
            location=dict(latitude=self.location[0], longitude=self.location[1])
        )

    @property
    def drone_id(self):
        return self._drone_id

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, new_status):
        if new_status in self.STATUSES:
            self._status = new_status
        else:
            raise AttributeError("Only one of {} allowed.".format(self.STATUSES))

    @property
    def eta(self):
        if self.status == IN_FLIGHT:
            # Simple date and time calculation.
            return datetime.datetime.today() + datetime.timedelta(hours=1)
        return None # Does not apply if Drone is not in flight.


class ToDictEncoder(json.JSONEncoder):
    def default(self, o):
        return o.to_dict()
